using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using HarmonyLib;
using RimWorld;
using UnityEngine;
using Verse;
using Verse.AI;

namespace PorousBoat.WorshipSacrifice
{
    [HarmonyPatch(typeof(Pawn_NeedsTracker), "AddOrRemoveNeedsAsAppropriate")]
    public class Pawn_NeedsTrackerPatches
    {
        static void Postfix(Pawn_NeedsTracker __instance, Pawn ___pawn)
        {
            var ideo = ___pawn?.Ideo;
            if (ideo != null && ideo.HasPrecept(DefOf_Worship.Worship_Required))
            {
                MethodInfo meth = AccessTools.Method(typeof(Pawn_NeedsTracker), "ShouldHaveNeed");
                bool shouldHave = (bool)meth.Invoke(__instance, new[] { DefOf_Worship.Worship });
                    
                if (shouldHave)
                {
                    MethodInfo _meth = AccessTools.Method(typeof(Pawn_NeedsTracker), "AddNeed");
                    meth.Invoke(__instance, new[] { DefOf_Worship.Worship });
                    if (___pawn.Ideo.HasPrecept(DefOf_Worship.Sacrifice_Required))
                    {
                        meth.Invoke(__instance, new[] { DefOf_Worship.Sacrifice });
                    }
                }
            }
        }
    }

    [HarmonyPatch(typeof(Pawn), nameof(Pawn.SpawnSetup))]
    public class Pawn_Patches
    {
        static void Postfix(Pawn __instance)
        {
            if (__instance.needs.TryGetNeed<Need_Sacrifice>() != null)
            {
                if (!WorshipSacrifice.sacrificeNeeds.ContainsKey(__instance.ideo.Ideo))
                {
                    WorshipSacrifice.sacrificeNeeds[__instance.ideo.Ideo] = new List<Need_Sacrifice>();
                }
                WorshipSacrifice.sacrificeNeeds[__instance.ideo.Ideo].Add(__instance.needs.TryGetNeed<Need_Sacrifice>());
            }
        }
    }

    [HarmonyPatch(typeof(TimeAssignmentSelector), "DrawTimeAssignmentSelectorGrid")]
    public class TimeAssignmentSelectorPatches
    {
        static void Postfix(Rect rect)
        {
            rect.yMax -= 2f;
            Rect rect2 = rect;
            rect2.xMax = rect2.center.x;
            rect2.yMax = rect2.center.y;
            // 0,0
            rect2.x += rect2.width;
            // 1,0
            rect2.y += rect2.height;
            rect2.x -= rect2.width;
            // 0,1
            rect2.x += rect2.width;
            // 1,1
            rect2.x += rect2.width;
            rect2.y -= rect2.height;
            // 2,0
            rect2.x += rect2.width;
            // 3,0
            MethodInfo meth = AccessTools.Method(typeof(TimeAssignmentSelector), "DrawTimeAssignmentSelectorFor");
            meth.Invoke(null, new object[] { rect2, TimeAssignmentDefOf_Worship.Worship });
        }
    }

    [HarmonyPatch(typeof(ThinkNode_Priority_GetJoy), "GetPriority")]
    public class ThinkNode_Priority_GetJoyPatches
    {
        static bool Prefix(Pawn pawn, ref float __result)
        {
            TimeAssignmentDef timeAssignmentDef = ((pawn.timetable == null) ? TimeAssignmentDefOf.Anything : pawn.timetable.CurrentAssignment);
            if (timeAssignmentDef == TimeAssignmentDefOf_Worship.Worship)
            {
                __result = 0f;
                return false;
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(JobGiver_Work), "GetPriority")]
    public class JobGiver_WorkPatches
    {
        static bool Prefix(Pawn pawn, ref float __result)
        {
            TimeAssignmentDef timeAssignmentDef = ((pawn.timetable == null) ? TimeAssignmentDefOf.Anything : pawn.timetable.CurrentAssignment);
            if (timeAssignmentDef == TimeAssignmentDefOf_Worship.Worship)
            {
                __result = 0f;
                return false;
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(JobGiver_GetRest), "GetPriority")]
    public class JobGiver_GetRestPatches
    {
        static bool Prefix(Pawn pawn, ref float __result)
        {
            if (pawn.RaceProps.Humanlike)
            {
                TimeAssignmentDef timeAssignmentDef = ((pawn.timetable == null) ? TimeAssignmentDefOf.Anything : pawn.timetable.CurrentAssignment);
                
                if (timeAssignmentDef == TimeAssignmentDefOf_Worship.Worship)
                {
                    float curLevel = pawn.needs.rest.CurLevel;
                    if (curLevel < 0.16f)
                    {
                        __result = 8f;
                    }

                    __result = 0f;
                    return false;
                }
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(JobDriver_Sacrifice), "MakeNewToils")]
    public class JobDriver_SacrificePatches
    {
        static void Postfix()
        {
            Find.HistoryEventsManager.RecordEvent(new HistoryEvent(DefOf_Worship.SacrificePerformed));
            Log.Message("Sarifice performed");
            //Toil recordEvent = ToilMaker.MakeToil("MakeNewToils");
            //recordEvent.initAction = delegate
            //{
            //    Find.HistoryEventsManager.RecordEvent(new HistoryEvent(DefOf_Worship.SacrificePerformed));
            //    Log.Message("Sarifice performed");
            //};
            //recordEvent.defaultCompleteMode = ToilCompleteMode.Instant;
            //foreach (var t in toils)
            //{
            //    yield return t;
            //}
            //yield return recordEvent;
        }
    }

    [HarmonyPatch(typeof(LordJob_Ritual_Duel), nameof(LordJob_Ritual_Duel.Notify_PawnLost))]
    public class LordJob_Ritual_DuelPatches
    {
        static void Postfix()
        {
            Find.HistoryEventsManager.RecordEvent(new HistoryEvent(DefOf_Worship.SacrificePerformed));
            Log.Message("Sarifice performed");
        }
    }
    
    public static class SacrificePatches {
        public static void Postfix(RitualOutcomeEffectWorker_FromQuality __instance, LordJob_Ritual jobRitual)
        {
            if (__instance.def.defName == "Sacrifice" || __instance.def.defName == "GladiatorDuel")
            {
                WorshipSacrifice.sacrificeNeeds.TryGetValue(jobRitual.Ritual.ideo).ForEach(n => n.CurLevel = n.MaxLevel);
            }
        }
    }
}