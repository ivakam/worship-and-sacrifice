using System.Collections.Generic;
using RimWorld;
using UnityEngine;
using Verse;

namespace PorousBoat.WorshipSacrifice
{
    public enum WorshipCategory : byte
    {
        NoConnection,
        SomeConnection,
        GoodConnection,
        DivineConnection,
    }
    
    public enum SacrificeCategory : byte
    {
        Bloodthirsty,
        Twitchy,
        Satisfied,
    }
    
    public class Need_Worship : Need
    {
        public int lastWorshipTick = -999;

        private int ticksAtZero;
        
        public float BaseWorshipFallPerTick => WorshipUtility.GetTickRate(WorshipSacrifice.Settings.worshipHoursToZero);

        public float BaseWorshipGainPerTick => WorshipUtility.GetTickRate(WorshipSacrifice.Settings.worshipHoursToFull);
        
        public const float ThreshWantsWorship = 0.4f;

        public const float ThresWorshipSatisfied = 0.8f;
        
        public WorshipCategory CurCategory
        {
            get
            {
                if (CurLevel < 0.01f)
                {
                    return WorshipCategory.NoConnection;
                }
                if (CurLevel < ThreshWantsWorship)
                {
                    return WorshipCategory.SomeConnection;
                }
                if (CurLevel < ThresWorshipSatisfied)
                {
                    return WorshipCategory.GoodConnection;
                }
                return WorshipCategory.DivineConnection;
            }
        }

        public bool WantsWorship => CurLevel < ThreshWantsWorship;
        public bool WorshipSatisfied => CurLevel > ThresWorshipSatisfied;

        public float WorshipFallPerTick => BaseWorshipFallPerTick;

        public override int GUIChangeArrow
        {
            get
            {
                if (Worshipping)
                {
                    return 1;
                }

                return -1;
            }
        }

        public int TickAtZero => ticksAtZero;

        public bool Worshipping => Find.TickManager.TicksGame < lastWorshipTick + 2;

        public Need_Worship(Pawn pawn) : base(pawn)
        {
            threshPercents = new List<float> { ThreshWantsWorship, ThresWorshipSatisfied };
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref ticksAtZero, "ticksAtZero", 0);
        }

        public override void SetInitialLevel()
        {
            CurLevel = Rand.Range(0.9f, 1f);
        }
        
        public override void NeedInterval()
        {
            if (!IsFrozen)
            {
                if (Worshipping)
                {
                    float roomOffset = 0f;
                    Room r = pawn.GetRoom();
                    if (r.Role == RoomRoleDefOf.WorshipRoom)
                    {
                        // Add an extra % of base worship based on room impressiveness. Caps at 50% bonus at 120 impressiveness.
                        // TODO: Move this to proper stat that can be displayed on room and such
                        roomOffset = Mathf.Min(1f, r.GetStat(RoomStatDefOf.Impressiveness) / 120f) * 0.5f;
                    }
                    CurLevel += 150f * (BaseWorshipGainPerTick + (roomOffset * BaseWorshipGainPerTick));
                }
                else
                {
                    CurLevel -= 150f * BaseWorshipFallPerTick;
                }
            }

            if (CurLevel < 0.0001f)
            {
                ticksAtZero += 150;
            }
            else
            {
                ticksAtZero = 0;
            }
        }
    }
    
    public class Need_Sacrifice : Need
    {
        public int lastSacrificeTick = -999;

        private int ticksAtZero;
        
        public float BaseSacrificeFallPerTick => WorshipUtility.GetTickRate(WorshipSacrifice.Settings.sacrificeHoursToZero);

        public readonly float ThreshWantsSacrifice = 0.2f;

        public readonly float ThresSacrificeSatisfied = 0.5f;
        
        public SacrificeCategory CurCategory
        {
            get
            {
                if (CurLevel < 0.01f)
                {
                    return SacrificeCategory.Bloodthirsty;
                }
                if (CurLevel < ThreshWantsSacrifice)
                {
                    return SacrificeCategory.Twitchy;
                }
                return SacrificeCategory.Satisfied;
            }
        }

        public bool WantsSacrifice => CurLevel < ThreshWantsSacrifice;
        public bool SacrificeSatisfied => CurLevel > ThresSacrificeSatisfied;

        public float SacrificeFallPerTick => BaseSacrificeFallPerTick;

        public override int GUIChangeArrow
        {
            get
            {
                return -1;
            }
        }

        public int TickAtZero => ticksAtZero;

        public Need_Sacrifice(Pawn pawn) : base(pawn)
        {
            threshPercents = new List<float> { ThreshWantsSacrifice, ThresSacrificeSatisfied };
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref ticksAtZero, "ticksAtZero", 0);
        }

        public override void SetInitialLevel()
        {
            CurLevel = 1f;
        }
        
        public override void NeedInterval()
        {
            if (!IsFrozen)
            {
                CurLevel -= 150 * BaseSacrificeFallPerTick;
            }

            if (CurLevel < 0.0001f)
            {
                ticksAtZero += 150;
            }
            else
            {
                ticksAtZero = 0;
            }
        }
    }
}