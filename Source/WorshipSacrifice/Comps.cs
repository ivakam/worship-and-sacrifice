using System.Collections.Generic;
using System.Linq;
using RimWorld;
using Verse;

namespace PorousBoat.WorshipSacrifice
{
    public class CompWorshipFocus : ThingComp
    {
        public override void PostSpawnSetup(bool respawningAfterLoad)
        {
            base.PostSpawnSetup(respawningAfterLoad);
            if (parent is Building b)
            {
                WorshipSacrifice.worshipFocuses.Add(b);
            }
        }

        public override void PostDeSpawn(Map map)
        {
            base.PostDeSpawn(map);
            if (parent is Building b)
            {
                WorshipSacrifice.worshipFocuses.Remove(b);
            }
        }
    }

    public class CompProperties_WorshipFocus : CompProperties
    {
        public CompProperties_WorshipFocus()
        {
            compClass = typeof(CompWorshipFocus);
        }
    }

    //public class WorshipFocusDef : Def
    //{
    //    public Ideo ideo;

    //    public bool CanPawnUse(Pawn p)
    //    {
    //        return p.Ideo.Equals(ideo);
    //    }
    //}
}