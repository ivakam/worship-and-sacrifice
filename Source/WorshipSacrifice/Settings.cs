using System;
using RimWorld;
using UnityEngine;
using Verse;

namespace PorousBoat.WorshipSacrifice
{
    public class WorshipSettings : ModSettings
    {
        public float worshipHoursToZero = 20f;
        public float worshipHoursToFull = 4.5f;
        public float sacrificeHoursToZero = 20f;

        public override void ExposeData()
        {
            Scribe_Values.Look(ref worshipHoursToZero, "worshipHoursToZero", 20f);
            Scribe_Values.Look(ref worshipHoursToFull, "worshipHoursToFull", 20f);
            Scribe_Values.Look(ref sacrificeHoursToZero, "sacrificeHoursToZero", 20f);
            base.ExposeData();
        }
    }
}