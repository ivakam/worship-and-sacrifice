﻿using System.Collections.Generic;
using System.Reflection;
using HarmonyLib;
using RimWorld;
using UnityEngine;
using Verse;

namespace PorousBoat.WorshipSacrifice
{
    [StaticConstructorOnStartup]
    public class WorshipSacrifice : Mod
    {
        private static WorshipSacrifice _instance;
        public static WorshipSettings Settings => _instance.GetSettings<WorshipSettings>();
        public static Harmony harmonyInstance;
        public static List<Building> worshipFocuses = new List<Building>();
        public static Dictionary<Ideo,List<Need_Sacrifice>> sacrificeNeeds = new Dictionary<Ideo,List<Need_Sacrifice>>();
            
        public WorshipSacrifice(ModContentPack content) : base(content)
        {
            Log.Message("WORSHIP & SACRIFICE: Running...");
            harmonyInstance = new Harmony("PorousBoat.WorshipSacrifice");
            harmonyInstance.PatchAll();
            List<MethodInfo> sacrificePatchTargets = new List<MethodInfo>
            {
                AccessTools.Method(typeof(RitualOutcomeEffectWorker_Duel), "ApplyExtraOutcome"),
                AccessTools.Method(typeof(RitualOutcomeEffectWorker_Consumable), "ApplyExtraOutcome"),
            };
            sacrificePatchTargets.ForEach(t => harmonyInstance.Patch(t, postfix: new HarmonyMethod(typeof(SacrificePatches), nameof(SacrificePatches.Postfix))));
            _instance = this;
        }

        public override void DoSettingsWindowContents(Rect inRect)
        {
            Listing_Standard lStandard = new Listing_Standard();
            lStandard.Begin(inRect);

            lStandard.Label($"{"WorshipHoursToZero".Translate()}: {Settings.worshipHoursToZero:n1}");
            Settings.worshipHoursToZero = lStandard.Slider(Settings.worshipHoursToZero, 0.1f, 50f);
            
            lStandard.Label($"{"WorshipHoursToFull".Translate()}: {Settings.worshipHoursToFull:n1}");
            Settings.worshipHoursToFull = lStandard.Slider(Settings.worshipHoursToFull, 0.1f, 50f);
            
            lStandard.Label($"{"SacrificeHoursToZero".Translate()}: {Settings.sacrificeHoursToZero:n1}");
            Settings.sacrificeHoursToZero = lStandard.Slider(Settings.sacrificeHoursToZero, 0.1f, 50f);
            
            lStandard.End();
            
            base.DoSettingsWindowContents(inRect);
        }

        public override string SettingsCategory()
        {
            return "WorshipModCategory".Translate();
        }
    }
}