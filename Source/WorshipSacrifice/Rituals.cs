using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HarmonyLib;
using RimWorld;
using Verse;
using Verse.AI.Group;

namespace PorousBoat.WorshipSacrifice
{
    public class LordJob_Ritual_Blessing : LordJob_Ritual
    {
        public LordJob_Ritual_Blessing()
        {
            
        }

        public LordJob_Ritual_Blessing(TargetInfo selectedTarget, Precept_Ritual ritual, RitualObligation obligation,
            List<RitualStage> allStages, RitualRoleAssignments assignments, Pawn organizer = null) 
            : base(selectedTarget, ritual, obligation, allStages, assignments, organizer)
        {
            
        }

        public override StateGraph CreateGraph()
        {
            FieldInfo f = AccessTools.Field(typeof(LordJob_Ritual), "stages");
            if (f == null || f.GetValue(this) == null)
            {
            }
            Log.Message("Creating graph, stages: " + f.GetValue(this));
            return base.CreateGraph();
        }
    }

    public class RitualBehaviorWorker_Blessing : RitualBehaviorWorker
    {
        public RitualBehaviorWorker_Blessing()
        {
            
        }

        public RitualBehaviorWorker_Blessing(RitualBehaviorDef def) : base(def)
        {
            
        }

        protected override LordJob CreateLordJob(TargetInfo target, Pawn organizer, Precept_Ritual ritual, RitualObligation obligation,
            RitualRoleAssignments assignments)
        {
            return new LordJob_Ritual_Blessing(target, ritual, obligation, def.stages, assignments, organizer);
        }
    }
    
    public class RitualRoleBlessee : RitualRole
    {
        public override bool AppliesToRole(Precept_Role role, out string reason, Precept_Ritual ritual = null, Pawn p = null,
            bool skipReason = false)
        {
            reason = null;
            if (ritual != null && p != null && p.Ideo != ritual.ideo)
            {
                if (!skipReason)
                {
                    reason = "MessageRitualRoleMustHaveIdeoToDoRole".Translate(
                        Find.ActiveLanguageWorker.WithIndefiniteArticle(ritual.ideo.memberName),
                        Find.ActiveLanguageWorker.WithIndefiniteArticle(Label));
                }
                return false;
            }
            if (p != null && !p.Faction.IsPlayerSafe())
            {
                if (!skipReason)
                {
                    reason = "MessageRitualRoleMustBeColonist".Translate(base.Label);
                }

                return false;
            }

            return true;
        }
    }

    public class RitualOutcomeEffectWorker_Blessing : RitualOutcomeEffectWorker_FromQuality
    {
        public static readonly Dictionary<string,ThingDef> BlessableWeapons = new Dictionary<string, ThingDef>
        {
            {"MeleeWeapon_Knife", ThingDefOf_Worship.MeleeWeapon_KnifeBladelink},
            {"MeleeWeapon_Club", ThingDefOf_Worship.MeleeWeapon_ClubBladelink},
            {"MeleeWeapon_Ikwa", ThingDefOf_Worship.MeleeWeapon_IkwaBladelink},
            {"MeleeWeapon_Spear", ThingDefOf_Worship.MeleeWeapon_SpearBladelink},
        };
        
        public RitualOutcomeEffectWorker_Blessing()
        {
            
        }

        public RitualOutcomeEffectWorker_Blessing(RitualOutcomeEffectDef def) : base(def)
        {
            
        }

        public static ThingDef ValidBlessingsForThing(ThingWithComps thing)
        {
            return BlessableWeapons.TryGetValue(thing.def.defName);
        }

        protected override void ApplyExtraOutcome(Dictionary<Pawn, int> totalPresence, LordJob_Ritual jobRitual, OutcomeChance outcome,
            out string extraOutcomeDesc, ref LookTargets letterLookTargets)
        {
            extraOutcomeDesc = null;
            if (!outcome.Positive)
            {
                return;
            }
            Pawn target = jobRitual.PawnWithRole("blessee");
            string deity = (target.ideo.Ideo.foundation is IdeoFoundation_Deity f && f.DeitiesListForReading.Count > 0)
                ? f.DeitiesListForReading.RandomElement().name
                : "a higher power";
            ThingWithComps weapon = target.equipment.Primary;
            if (weapon == null)
            {
                extraOutcomeDesc = "RitualOutcomeExtraDesc_PawnBlessed".Translate(deity);
                target.health.AddHediff(DefOf_Worship.Blessing);
                Find.LetterStack.ReceiveLetter("LetterLabelPawnBlessed".Translate(), "LetterPawnBlessed".Translate(deity), LetterDefOf.PositiveEvent, new LookTargets(target));
                return;
            }

            ThingDef bladeLinkDef = ValidBlessingsForThing(weapon);
            if (bladeLinkDef != null)
            {
                Thing bladeLink = ThingMaker.MakeThing(bladeLinkDef, weapon.Stuff);
                bladeLink.TryGetComp<CompQuality>().SetQuality(weapon.TryGetComp<CompQuality>().Quality, ArtGenerationContext.Colony);
                bladeLink = GenSpawn.Spawn(bladeLink, jobRitual.Spot, target.Map);
                weapon.Destroy();
                extraOutcomeDesc = "RitualOutcomeExtraDesc_WeaponBlessed".Translate(deity);
                Find.LetterStack.ReceiveLetter("LetterLabelWeaponBlessed".Translate(), "LetterWeaponBlessed".Translate(deity), LetterDefOf.PositiveEvent, new LookTargets(bladeLink));
            }
        }
    }

    public class StageEndTrigger_BlesseeRole : StageEndTrigger
    {
        [NoTranslate] public string roleId;
        public override Trigger MakeTrigger(LordJob_Ritual ritual, TargetInfo spot, IEnumerable<TargetInfo> foci, RitualStage stage)
        {
            return new Trigger_Custom(delegate { return true; });
        }
    }

    public class RitualOutcomeComp_PawnWorship : RitualOutcomeComp_Quality
    {
        
        public override float Count(LordJob_Ritual ritual, RitualOutcomeComp_Data data)
        {
            return WorshipUtility.GetAvgWorship(ritual.Ritual.ideo);
        }

        public override ExpectedOutcomeDesc GetExpectedOutcomeDesc(Precept_Ritual ritual, TargetInfo ritualTarget, RitualObligation obligation,
            RitualRoleAssignments assignments, RitualOutcomeComp_Data data)
        {
            float avg = WorshipUtility.GetAvgWorship(ritual.ideo);
            float val = curve.Evaluate(avg);
            return new ExpectedOutcomeDesc
            {
                label = label.CapitalizeFirst(),
                count = avg.ToString("n2") + " / " + MaxValue,
                effect = ExpectedOffsetDesc(positive: true, val),
                quality = val,
                positive = (val > 0),
                priority = 10f
            };
        }
    }
}