using RimWorld;
using Verse;

namespace PorousBoat.WorshipSacrifice
{
    //[DefOf]
    //public class MemeDefOf_Custom
    //{
    //    public static MemeDef GodFearing;

    //    static MemeDefOf_Custom()
    //    {
    //        DefOfHelper.EnsureInitializedInCtor(typeof(MemeDefOf_Custom));
    //    }
    //}
    
    [DefOf]
    public class DefOf_Worship
    {
        public static PreceptDef Sacrifice_Abhorrent;
        public static PreceptDef Sacrifice_Neutral;
        public static PreceptDef Sacrifice_Required;
        public static PreceptDef Worship_Neutral;
        public static PreceptDef Worship_Required;
        
        public static NeedDef Worship;
        public static NeedDef Sacrifice;

        public static HediffDef Blessing;

        public static HistoryEventDef SacrificePerformed;
        
        //public static StatDef WorshipStrength;

        static DefOf_Worship()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(DefOf_Worship));
        }
    }

    [DefOf]
    public class ThingDefOf_Worship
    {
        public static ThingDef MeleeWeapon_ClubBladelink;
        public static ThingDef MeleeWeapon_KnifeBladelink;
        public static ThingDef MeleeWeapon_IkwaBladelink;
        public static ThingDef MeleeWeapon_SpearBladelink;

        static ThingDefOf_Worship()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(ThingDefOf_Worship));
        }
    }

    [DefOf]
    public class TimeAssignmentDefOf_Worship
    {
        public static TimeAssignmentDef Worship;
        
        static TimeAssignmentDefOf_Worship()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(TimeAssignmentDefOf_Worship));
        }
    }

    [DefOf]
    public class JobDefOf_Worship
    {
        public static JobDef Worship;
        public static JobDef Bless;

        static JobDefOf_Worship()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(JobDefOf_Worship));
        }
    }
}