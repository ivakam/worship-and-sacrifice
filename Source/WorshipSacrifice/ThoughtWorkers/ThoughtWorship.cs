using RimWorld;
using Verse;

namespace PorousBoat.WorshipSacrifice
{
    public class ThoughtWorker_WantsWorship : ThoughtWorker
    {
        protected override ThoughtState CurrentStateInternal(Pawn p)
        {
            Need_Worship _n = p.needs.TryGetNeed<Need_Worship>();
            bool hasReqPrecept = p.ideo.Ideo.HasPrecept(DefOf_Worship.Worship_Required);
            if (hasReqPrecept && _n is Need_Worship n)
            {
                if (!n.WantsWorship)
                {
                    return ThoughtState.Inactive;
                }
                return ThoughtState.ActiveAtStage(0);
            }
            return ThoughtState.Inactive;
        }
    }
    
    public class ThoughtWorker_WorshipSatisfied : ThoughtWorker
    {
        protected override ThoughtState CurrentStateInternal(Pawn p)
        {
            Need_Worship _n = p.needs.TryGetNeed<Need_Worship>();
            if (_n is Need_Worship n)
            {
                if (!n.WorshipSatisfied)
                {
                    return ThoughtState.Inactive;
                }

                return (p.ideo.Ideo.HasPrecept(DefOf_Worship.Worship_Required))
                    ? ThoughtState.ActiveAtStage(1)
                    : ThoughtState.ActiveAtStage(0);
            }
            return ThoughtState.Inactive;
        }
    }
}