using RimWorld;
using Verse;

namespace PorousBoat.WorshipSacrifice
{
    public class ThoughtWorker_WantsSacrifice : ThoughtWorker
    {
        protected override ThoughtState CurrentStateInternal(Pawn p)
        {
            Need_Sacrifice _n = p.needs.TryGetNeed<Need_Sacrifice>();
            bool hasReqPrecept = p.ideo.Ideo.HasPrecept(DefOf_Worship.Sacrifice_Required);
            if (hasReqPrecept && _n is Need_Sacrifice n)
            {
                if (!n.WantsSacrifice)
                {
                    return ThoughtState.Inactive;
                }
                return ThoughtState.ActiveAtStage(0);
            }
            return ThoughtState.Inactive;
        }
    }
}