using System;
using RimWorld;
using Verse;
using Verse.AI;

namespace PorousBoat.WorshipSacrifice
{
    public class JobGiver_Worship : ThinkNode_JobGiver
    {
        private WorshipCategory minCategory;
        private float maxLevelPercentage = 1f;

        public override ThinkNode DeepCopy(bool resolve = true)
        {
            JobGiver_Worship obj = (JobGiver_Worship)base.DeepCopy(resolve);
            obj.minCategory = minCategory;
            obj.maxLevelPercentage = maxLevelPercentage;
            return obj;
        }

        public override float GetPriority(Pawn pawn)
        {
            Need_Worship worship = pawn.needs.TryGetNeed<Need_Worship>();
            if (worship == null)
            {
                return 0f;
            }
            if ((int)worship.CurCategory < (int)minCategory)
            {
                return 0f;
            }

            if (worship.CurLevelPercentage > maxLevelPercentage)
            {
                return 0f;
            }

            if (!WorshipUtility.CanWorshipNow(pawn))
            {
                return 0f;
            }

            TimeAssignmentDef timeDef;
            if (pawn.RaceProps.Humanlike)
            {
                timeDef = ((pawn.timetable == null)
                    ? TimeAssignmentDefOf_Worship.Worship
                    : pawn.timetable.CurrentAssignment);
            }
            else
            {
                return 0f;
            }
            
            float curLevel = worship.CurLevel;

            if (timeDef == TimeAssignmentDefOf_Worship.Worship)
            {
                if (curLevel < 0.95f)
                {
                    return 7f;
                }

                return 0f;
            }

            bool hasReqPrecept = pawn.ideo.Ideo.HasPrecept(DefOf_Worship.Worship_Required);

            if (timeDef == TimeAssignmentDefOf.Anything)
            {
                if (hasReqPrecept && curLevel < 0.3f)
                {
                    return 7f;
                }

                return 0f;
            }

            if (timeDef == TimeAssignmentDefOf.Work)
            {
                return 0f;
            }

            if (timeDef == TimeAssignmentDefOf.Meditate)
            {
                if (hasReqPrecept && curLevel < 0.15)
                {
                    return 7f;
                }

                return 0f;
            }

            if (timeDef == TimeAssignmentDefOf.Joy)
            {
                if (hasReqPrecept && curLevel < 0.4f)
                {
                    return 7f;
                }

                return 0f;
            }

            if (timeDef == TimeAssignmentDefOf.Sleep)
            {
                if (hasReqPrecept && curLevel < 0.3f)
                {
                    return 7f;
                }

                return 0f;
            }

            return 0f;
        }

        protected override Job TryGiveJob(Pawn pawn)
        {
            Need_Worship worship = pawn.needs.TryGetNeed<Need_Worship>();
            if (worship == null || (int)worship.CurCategory < (int)minCategory || worship.CurLevelPercentage > maxLevelPercentage)
            {
                return null;
            }

            if (!WorshipUtility.CanWorshipNow(pawn))
            {
                return null;
            }

            return WorshipUtility.GetWorshipJob(pawn);
        }
    }
}