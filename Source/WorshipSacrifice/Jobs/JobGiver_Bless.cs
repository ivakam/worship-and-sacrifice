using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace PorousBoat.WorshipSacrifice
{
    public class JobGiver_Bless : ThinkNode_JobGiver
    {
        protected override Job TryGiveJob(Pawn pawn)
        {
            Lord lord = pawn.GetLord();
            if (lord == null || !(lord.LordJob is LordJob_Ritual_Blessing lordJobRitualBlessing))
            {
                return null;
            }

            Pawn pawn2 = pawn.mindState.duty.focusSecond.Pawn;
            if (!pawn.CanReserveAndReach(pawn2, PathEndMode.ClosestTouch, Danger.None))
            {
                return null;
            }

            return JobMaker.MakeJob(JobDefOf_Worship.Bless, pawn2, pawn.mindState.duty.focus);
        }
    }
}