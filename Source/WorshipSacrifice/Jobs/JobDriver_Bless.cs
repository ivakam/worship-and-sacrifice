using System.Collections.Generic;
using UnityEngine;
using Verse;
using Verse.AI;

namespace PorousBoat.WorshipSacrifice
{
    public class JobDriver_Bless : JobDriver
    {
        protected Pawn Target => (Pawn)job.GetTarget(TargetIndex.A).Thing;
        
        public override bool TryMakePreToilReservations(bool errorOnFailed)
        {
            return pawn.Reserve(Target, job, 1, -1, null, errorOnFailed);
        }

        protected override IEnumerable<Toil> MakeNewToils()
        {
            this.FailOnDespawnedOrNull(TargetIndex.A);
            Pawn target = Target;
            yield return Toils_Goto.GotoThing(TargetIndex.A, PathEndMode.ClosestTouch);
            yield return Toils_General.Wait(35);
            // TODO: sfx and other stuff that goes with the actual "blessing"
        }
    }
}