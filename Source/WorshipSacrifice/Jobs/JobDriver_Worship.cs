using System.Collections.Generic;
using System.Security.Cryptography;
using RimWorld;
using Verse;
using Verse.AI;
using Verse.Sound;

namespace PorousBoat.WorshipSacrifice
{
    public class JobDriver_Worship : JobDriver
    {
        protected IntVec3 faceDir;
        protected Sustainer sustainer;
        protected const TargetIndex SpotInd = TargetIndex.A;
        protected const TargetIndex BedInd = TargetIndex.B;
        protected const TargetIndex FocusInd = TargetIndex.C;
        public LocalTargetInfo Focus => job.GetTarget(TargetIndex.C);
        private bool FromBed => job.GetTarget(TargetIndex.B).IsValid;

        // TODO: Worship per day report
        
        public override string GetReport()
        {
            if (ModsConfig.IdeologyActive)
            {
                Thing thing = Focus.Thing;
                if (thing != null && !thing.Destroyed)
                {
                    return "WorshippingAt".Translate() + ": " + thing.LabelShort.CapitalizeFirst() + ".";
                }
            }

            return base.GetReport();
        }

        public override bool TryMakePreToilReservations(bool errorOnFailed)
        {
            return pawn.Reserve(job.GetTarget(TargetIndex.A), job, 1, -1, null, errorOnFailed);
        }

        protected override IEnumerable<Toil> MakeNewToils()
        {
            Toil worship = ToilMaker.MakeToil("MakeNewToils");
            worship.socialMode = RandomSocialMode.Off;
            if (FromBed)
            {
                this.KeepLyingDown(TargetIndex.B);
                worship = Toils_LayDown.LayDown(TargetIndex.B, job.GetTarget(TargetIndex.B).Thing is Building_Bed,
                    lookForOtherJobs: false, canSleep: false);
            }
            else
            {
                yield return Toils_Goto.GotoCell(TargetIndex.A, PathEndMode.OnCell);
                worship.initAction = delegate
                {
                    LocalTargetInfo target = job.GetTarget(TargetIndex.C);
                    if (target.IsValid)
                    {
                        faceDir = target.Cell - pawn.Position;
                    }
                    else
                    {
                        faceDir = (job.def.faceDir.IsValid ? job.def.faceDir : Rot4.Random).FacingCell;
                    }
                };
                if (Focus != null)
                {
                    worship.FailOnDespawnedOrNull(TargetIndex.C);
                    worship.FailOnForbidden(TargetIndex.A);
                }
                worship.handlingFacing = true;
            }

            worship.defaultCompleteMode = ToilCompleteMode.Delay;
            worship.defaultDuration = job.def.joyDuration;
            worship.FailOn(() =>
                !MeditationUtility.CanMeditateNow(pawn) ||
                !MeditationUtility.SafeEnvironmentalConditions(pawn, base.TargetLocA, base.Map));
            worship.AddPreTickAction(delegate
            {
                bool isWorshipTime = pawn.GetTimeAssignment() == TimeAssignmentDefOf_Worship.Worship;
                // TODO: Possible bad stuff with job.wasOnMeditationTimeAssignment flag
                if (faceDir.IsValid && !FromBed)
                {
                    pawn.rotationTracker.FaceCell(pawn.Position + faceDir);
                }

                WorshipTick();
            });
            yield return worship;
        }

        protected void WorshipTick()
        {
            pawn.GainComfortFromCellIfPossible();
            if (pawn.needs.joy != null)
            {
                JoyUtility.JoyTickCheckEnd(pawn, JoyTickFullJoyAction.None);
            }

            if (pawn.IsHashIntervalTick(100))
            {
                //TODO: Add fleck
                //FleckMaker.ThrowMetaIcon(pawn.Position, pawn.Map, FleckDefOf_Custom.Worshipping);
            }

            if (!ModsConfig.IdeologyActive)
            {
                return;
            }

            if (pawn.needs.TryGetNeed<Need_Worship>() != null)
            {
                pawn.needs.TryGetNeed<Need_Worship>().lastWorshipTick = Find.TickManager.TicksGame;
            }
            //TODO: Sound effects etc.
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref faceDir, "faceDir");
        }
    }
}