using System;
using System.Collections.Generic;
using System.Linq;
using RimWorld;
using UnityEngine;
using Verse;
using Verse.AI;

namespace PorousBoat.WorshipSacrifice
{
    public struct WorshipSpotAndFocus
    {
        public LocalTargetInfo spot;
        public LocalTargetInfo focus;
        public bool IsValid => spot.IsValid;

        public WorshipSpotAndFocus(LocalTargetInfo spot)
        {
            this.spot = spot;
            focus = LocalTargetInfo.Invalid;
        }

        public WorshipSpotAndFocus(LocalTargetInfo spot, LocalTargetInfo focus)
        {
            this.spot = spot;
            this.focus = focus;
        }
    }
    
    public class WorshipUtility
    {
        public static float FocusObjectSearchRadius = 3.9f;
        private static float WanderRadius = 10f;

        public static Job GetWorshipJob(Pawn pawn, bool forjoy = false)
        {
            WorshipSpotAndFocus worshipSpotAndFocus = FindWorshipSpot(pawn);
            if (worshipSpotAndFocus.IsValid)
            {
                Job job;
                JobDef def = JobDefOf_Worship.Worship;
                job = JobMaker.MakeJob(def, worshipSpotAndFocus.spot, null, worshipSpotAndFocus.focus);
                job.ignoreJoyTimeAssignment = !forjoy;
                return job;
            }

            return null;
        }

        public static float GetTickRate(float hours) => 1 / (hours * 2500);
        
        public static float GetAvgWorship(Ideo ideo)
        {
            List<Pawn> colonists = PawnsFinder.AllMaps_FreeColonists
                .Where(p => p.ideo.Ideo == ideo)
                .ToList();
            List<Need_Worship> worshipNeeds = colonists.Select(p => p.needs.TryGetNeed<Need_Worship>())
                .Where(n => n != null).ToList();
            return worshipNeeds.Select(n => n.CurLevelPercentage).Sum() / worshipNeeds.Count;
        }

        public static WorshipSpotAndFocus FindWorshipSpot(Pawn pawn)
        {
            float distance = float.MinValue;
            LocalTargetInfo spot = LocalTargetInfo.Invalid;
            LocalTargetInfo focus = LocalTargetInfo.Invalid;
            Room ownedRoom = pawn.ownership.OwnedRoom;
            foreach (LocalTargetInfo item in AllWorshipSpotCandidates(pawn))
            {
                if (!SafeEnvironmentalConditions(pawn, item.Cell, pawn.Map) || !item.Cell.Standable(pawn.Map) || item.Cell.IsForbidden(pawn))
                {
                    continue;
                }

                if (item.Pawn != null && item.Pawn != pawn)
                {
                    continue;
                }
                
                float num2 = 1f / Mathf.Max(item.Cell.DistanceToSquared(pawn.Position), 0.1f);
                LocalTargetInfo localTargetInfo = ((item.Thing.TryGetComp<CompWorshipFocus>() != null)
                    ? ((LocalTargetInfo)item.Thing)
                    : BestWorshipAt(item, pawn));
                Room room = item.Cell.GetRoom(pawn.Map);
                if (room != null && ownedRoom == room)
                {
                    num2 += 1f;
                }

                if (room != null && ModsConfig.IdeologyActive && room.Role == RoomRoleDefOf.WorshipRoom)
                {
                    num2 += 100f;
                    foreach (Thing containedAndAdjacentThing in room.ContainedAndAdjacentThings)
                    {
                        num2 += containedAndAdjacentThing.GetStatValue(StatDefOf.StyleDominance);
                    }
                }
                
                if (num2 > distance)
                {
                    spot = item;
                    focus = localTargetInfo;
                    distance = num2;
                }
            }

            return new WorshipSpotAndFocus(spot, focus);
        }

        public static bool IsValidWorshipBuildingForPawn(Building spot, Pawn pawn)
        {
            if (spot.DestroyedOrNull() || spot.IsForbidden(pawn) || !spot.Position.Standable(spot.Map))
            {
                return false;
            }

            if (spot.GetAssignedPawn() != null && spot.GetAssignedPawn() != pawn)
            {
                return false;
            }

            Room room = spot.GetRoom();
            if (room != null && !CanUseRoomToWorship(room, pawn))
            {
                return false;
            }

            if (spot.TryGetComp<CompWorshipFocus>() == null)
            {
                //return false;
            }

            return pawn.CanReserveAndReach(spot, PathEndMode.OnCell, pawn.NormalMaxDanger());
        }

        public static bool SafeEnvironmentalConditions(Pawn pawn, IntVec3 cell, Map map)
        {
            if (map.gameConditionManager.ConditionIsActive(GameConditionDefOf.ToxicFallout) && !cell.Roofed(map))
            {
                return false;
            }

            if (ModsConfig.BiotechActive && NoxiousHazeUtility.IsExposedToNoxiousHaze(pawn, cell, map))
            {
                return false;
            }

            if (cell.GetDangerFor(pawn, map) != Danger.None)
            {
                return false;
            }

            return true;
        }

        public static bool CanWorshipNow(Pawn pawn)
        {
            if (pawn.needs.rest != null && (int)pawn.needs.rest.CurCategory >= 2)
            {
                return false;
            }

            if (pawn.needs.food != null && pawn.needs.food.Starving)
            {
                return false;
            }

            if (pawn.Downed || !pawn.Awake())
            {
                return false;
            }

            if (pawn.health.hediffSet.BleedRateTotal > 0f ||
                (HealthAIUtility.ShouldSeekMedicalRest(pawn) &&
                 pawn.timetable?.CurrentAssignment != TimeAssignmentDefOf_Worship.Worship) ||
                HealthAIUtility.ShouldSeekMedicalRestUrgent(pawn))
            {
                return false;
            }

            return true;
        }
        
        public static IEnumerable<LocalTargetInfo> AllWorshipSpotCandidates(Pawn pawn)
        {
            IntVec3 intVec;
            
            foreach (Room room in UsableWorshipRooms(pawn))
            {
                foreach (LocalTargetInfo fSpot in WorshipSpotsInTheRoom(pawn, room))
                {
                    if (pawn.CanReach(fSpot, PathEndMode.Touch, pawn.NormalMaxDanger()))
                    {
                        yield return fSpot;
                    }
                }

                IntVec3 randomCell = room.Districts.RandomElement().Regions.RandomElement().RandomCell;
                intVec = RCellFinder.RandomWanderDestFor(pawn, randomCell, WanderRadius,
                    (Pawn p, IntVec3 c, IntVec3 r) => c.GetRoom(p.Map) == room &&
                                                      c.Standable(p.Map) &&
                                                      c.GetDoor(p.Map) == null &&
                                                      WanderRoomUtility.IsValidWanderDest(p, c, r),
                    pawn.NormalMaxDanger());
                if (intVec.IsValid)
                {
                    yield return intVec;
                }
            }

            Building_Bed bed = pawn.ownership.OwnedBed;
            Room bedroom = bed?.GetRoom();
            if (bedroom != null && !bedroom.PsychologicallyOutdoors &&
                pawn.CanReserveAndReach(bed, PathEndMode.OnCell, pawn.NormalMaxDanger()))
            {
                foreach (LocalTargetInfo focus in WorshipSpotsInTheRoom(pawn, bedroom))
                {
                    yield return focus;
                }

                intVec = RCellFinder.RandomWanderDestFor(pawn, bed.Position, WanderRadius,
                    (Pawn p, IntVec3 c, IntVec3 r) => c.Standable(p.Map) &&
                                                      c.GetDoor(p.Map) == null &&
                                                      WanderRoomUtility.IsValidWanderDest(p, c, r), pawn.NormalMaxDanger());
                if (intVec.IsValid)
                {
                    yield return intVec;
                }
            }

            if (pawn.IsPrisonerOfColony)
            {
                yield break;
            }

            IntVec3 colonyWanderRoot = WanderUtility.GetColonyWanderRoot(pawn);
            intVec = RCellFinder.RandomWanderDestFor(pawn, colonyWanderRoot, WanderRadius,
            delegate(Pawn p, IntVec3 c, IntVec3 r)
            {
                if (!c.Standable(p.Map) || c.GetDoor(p.Map) != null ||
                    !p.CanReserveAndReach(c, PathEndMode.OnCell, p.NormalMaxDanger()))
                {
                    return false;
                }

                Room room4 = c.GetRoom(p.Map);
                return (room4 == null || CanUseRoomToWorship(room4, pawn));
            }, pawn.NormalMaxDanger());
            if (intVec.IsValid)
            {
                yield return intVec;
            }
        }

        public static IEnumerable<Room> UsableWorshipRooms(Pawn pawn)
        {
            foreach (Room allRoom in pawn.Map.regionGrid.allRooms)
            {
                if (allRoom.Role == RoomRoleDefOf.WorshipRoom && CanUseRoomToWorship(allRoom, pawn))
                {
                    yield return allRoom;
                }
            }
        }

        public static LocalTargetInfo BestWorshipAt(LocalTargetInfo spot, Pawn pawn)
        {
            float num = 0f;
            LocalTargetInfo result = LocalTargetInfo.Invalid;
            foreach (Thing item in GenRadial.RadialDistinctThingsAround(spot.Cell, pawn.MapHeld,
                         FocusObjectSearchRadius, useCenter: false))
            {
                if (!GenSight.LineOfSightToThing(spot.Cell, item, pawn.Map) || item.TryGetComp<CompWorshipFocus>() != null)
                {
                    continue;
                }

                CompWorshipFocus compWorshipFocus = item.TryGetComp<CompWorshipFocus>();
                if (compWorshipFocus != null)
                {
                    // TODO: Fix worship strength
                    //float statValueForPawn = item.GetStatValueForPawn(DefOf_Worship.WorshipStrength, pawn);
                    float statValueForPawn = 1f;
                    if (statValueForPawn > num)
                    {
                        result = item;
                        num = statValueForPawn;
                    }
                }
            }

            return result;
        }
        public static IEnumerable<LocalTargetInfo> WorshipSpotsInTheRoom(Pawn pawn, Room r)
        {
            List<Thing> things = r.ContainedAndAdjacentThings;
            for (int i = 0; i < things.Count; i++)
            {
                CompWorshipFocus compWorshipFocus = things[i].TryGetComp<CompWorshipFocus>();
                if (compWorshipFocus != null)
                {
                    LocalTargetInfo localTargetInfo = WorshipSpotForFocus(things[i], pawn, r.ContainsCell);
                    if (localTargetInfo.IsValid)
                    {
                        yield return localTargetInfo;
                    }
                }
            }
        }

        public static LocalTargetInfo WorshipSpotForFocus(Thing t, Pawn p, Func<IntVec3, bool> validator = null)
        {
            return t.OccupiedRect().ExpandedBy(2).AdjacentCellsCardinal.Where((IntVec3 cell) =>
                    (validator == null || validator(cell)) &&
                    !cell.IsForbidden(p) &&
                    p.CanReserveAndReach(cell, PathEndMode.OnCell, p.NormalMaxDanger()) &&
                    cell.Standable(p.Map))
                .RandomElementWithFallback(IntVec3.Invalid);
        }

        public static bool CanUseRoomToWorship(Room r, Pawn p)
        {
            if (!r.Owners.EnumerableNullOrEmpty() && !r.Owners.Contains(p))
            {
                return false;
            }

            if (r.IsPrisonCell && !p.IsPrisoner)
            {
                return false;
            }

            return true;
        }
    }

}